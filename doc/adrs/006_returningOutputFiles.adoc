// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: CC-BY-ND-4.0

= ADR 006: Returning Output Files

== Context

Users request CPAchecker runs at this web service.
These runs produce output files, such as statistics files or verification witnesses.
We want to be able to communicate these files to the user.
Files have a size between a few kB and a few MB.
We identify files through their base name. For example, for output file
"/tmp/cpa-daemon/run-1/output/Statistics.txt", the basename is Statistics.txt.

There are use cases in which we need multiple output files:
Statistics.txt + verification witness, CFA.dot + ARG.dot, etc.

We have to decide how the user can ask for multiple output files,
and how file content is communicated to the user.


== Decision

We decide to use a single get-request with multiple file names.

We decide to directly send the file content in the response message.


== Details

=== Asking for output files

To receive multiple output files for a Run, the user can:

* send multiple get-requests with a single file name each, or
* send a single get-request with multiple file names.

Multiple get requests with a single file name each keep the request- and response data structure 
and the logic in the server simpler.
But it produces significantly more requests against the server and
gives the server less control over how to concurrently handle a batch of multiple files.

A single get-request with multiple file names makes the request- and response data structure
and the logic _slightly_ more complex: The request data structure has to allow multiple file names,
and a single response must be able to contain more than one file content.
In addition, a single Protobuf Message cannot be larger than 2GB:
For many files of multiple MB in size, this limit could be reached, in theory.
But for a single CPAchecker run, it is extremely unlikely that all output files together reach that limit.

As an advantage, the server has more control over how to handle the batch of files.

We decide to use a single get-request with multiple file names.
The slight increase in complexity is worth the additional flexibility and less requests.

=== Communicating file content

To return file content ot the user, we can:

* send the file content in the response message, or
* send a download link in the response message.

Sending the file content in the response message requires less calls by the user,
but the user has to handle writing the file content explicitly.
In addition, there is the theoretical limit of 2GB per response message.

Sending a download link in the response message requires more calls by the user,
but there is no file limit and the use of a file URL is more intuitive for the user.
But the setup is more copmlex: we have to set up an additional HTTP server to host the files for download,
and provide all created output files to that HTTP server.

We decide to directly send the file content in the response message, because it is
the simpler setup.



