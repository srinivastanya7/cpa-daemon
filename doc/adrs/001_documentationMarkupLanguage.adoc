// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: CC-BY-ND-4.0

= ADR 001: Documentation Markup Language

== Context

We have to decide on a markup language to write our documentation in,
so that we can compile all documentation with a single tool.

== Decision

We use AsciiDoc (https://docs.asciidoctor.org/asciidoc/latest/[official documentation]).

== Details

We identified three viable markup languages for our documentation:

* https://www.markdownguide.org/[Markdown] in https://pandoc.org/[pandoc] flavor
* https://docutils.sourceforge.io/rst.html[ReStructuredText]
* https://asciidoc.org/[AsciiDoc]

*Markdown* is widely used in online-collaboration tools and well-known by developers,
but it only supports basic formatting. For example,
in the language itself, there is only little support for generating tables of contents
and image formatting. There is no support for admonitions like notes or warnings.

*ReStructuredText* is feature-rich, but its syntax is sometimes esoteric and quite verbose.
In addition, markup for sections, subsections etc.
does not directly show the heading depth and are free to choose,
so it is difficult to keep track of the current section depth in longer documents.
Examples:

[,rst]
----

Some section heading
====================

There is no way to directly tell the current section depth. It is also not clear whether
the following section is above or below this section.

........................
Another section heading
........................

Links are complicated to write:
`Repository link <https://gitlab.com/sosy-lab/software/cpa-daemon/>`_

::

  Preformatted text must be indented by two spaces _and_ follow a text block that ends with
  the :: marker.

Code-blocks are visually equal to preformatted text,
and only recognizable as code-blocks through their name:

.. code-block:: java

   int x = 0;

----

*AsciiDoc* is feature-rich as well, and is closer to the widely used Markdown style
of markup. It does not rely on spacing and has a clearly defined syntax for many
formatting constructs.
See this document's source for an example.

[,adoc]
----

= Some section heading

AsciiDoc clearly shows that this is a first-level section, and that the following section
is a second-level section.

== Another section heading

Links are easier to write:
https://gitlab.com/sosy-lab/software/cpa-daemon/[Repository link]

 Preformatted text must be indented by one space. It is the only text element
 that relies on a precise indentation.

Code-blocks are visually different to preformatted text:

[,java]
----
   int x = 0;
----

----

So we choose AsciiDoc for our documentation documents.
