// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import jakarta.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.sosy_lab.cpachecker.grpc.runs.RunManager;

/** This class signals the health of the service. */
@Liveness
public class LivenessProbe implements HealthCheck {

  private final RunManager runManager;
  private final Integer threshold;

  @Inject
  public LivenessProbe(
      RunManager runManager,
      @ConfigProperty(name = "cpachecker.grpc.restartThreshold") int threshold) {
    this.runManager = runManager;
    this.threshold = threshold;
  }

  @Override
  public HealthCheckResponse call() {
    if (threshold == -1) {
      return HealthCheckResponse.up("Restarts");
    }

    if (runManager.getFinishedRunCount() >= threshold) {
      return HealthCheckResponse.down("Restarts");
    }

    return HealthCheckResponse.up("Restarts");
  }
}
