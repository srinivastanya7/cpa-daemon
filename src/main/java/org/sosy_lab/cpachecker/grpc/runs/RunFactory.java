// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

/** A factory of CPAchecker runs. */
public interface RunFactory {

  /**
   * Creates a new CPAchecker run.
   *
   * @param name a unique identifier for the new run
   */
  Run create(String name);
}
