// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import io.quarkus.arc.properties.IfBuildProperty;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.nio.file.Path;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunFactory;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** Factory that creates CPAchecker runs that run as native binaries. */
@Singleton
@IfBuildProperty(name = "cpachecker.grpc.run.native.enabled", stringValue = "true")
public class NativeBinaryRunFactory implements RunFactory {

  private final ProcessOptions processOptions;
  private final CpacheckerSupport cpacheckerSupport;
  private final RunConfigMapper configMapper;
  private final DirectoryManager directoryManager;
  private final CpacheckerProcessBuilder processBuilder;
  private final Path nativeBinaryPath;

  /**
   * Creates a new instance of this class.
   *
   * @param processOptions options for the process that runs CPAchecker
   * @param cpacheckerSupport utility class for handling CPAchecker
   * @param configMapper config mapper
   * @param directoryManager manager for working directories of CPAchecker runs
   * @param processBuilder builder for CPAchecker processes
   * @param nativeBinaryPath path to the native CPAchecker binary
   */
  @Inject
  NativeBinaryRunFactory(
      ProcessOptions processOptions,
      CpacheckerSupport cpacheckerSupport,
      RunConfigMapper configMapper,
      DirectoryManager directoryManager,
      CpacheckerProcessBuilder processBuilder,
      @ConfigProperty(name = "cpachecker.grpc.run.native.binary") Path nativeBinaryPath) {
    this.processOptions = processOptions;
    this.cpacheckerSupport = cpacheckerSupport;
    this.configMapper = configMapper;
    this.directoryManager = directoryManager;
    this.processBuilder = processBuilder;
    this.nativeBinaryPath = nativeBinaryPath;
  }

  @Override
  public Run create(String name) {
    return new NativeBinaryRun(
        name,
        processOptions,
        configMapper,
        cpacheckerSupport,
        directoryManager,
        processBuilder,
        nativeBinaryPath);
  }
}
