// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import com.google.common.base.Strings;
import jakarta.inject.Singleton;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** Mapper from a run request to a run config. */
@Singleton
public class RunConfigMapper {

  private static final String DEFAULT_ANALYSIS_ENTRY = "main";
  private static final CharSequence CPACHECKER_OPTION_VALUE_DELIMITER = ",";

  /**
   * Maps the given CPAchecker run request to a run config. The run config will not represent all
   * options of the run request, but only those that we represent in the config.
   *
   * @param request the run request to map to a run config
   * @param runDirectory the directory in which the configured run should store input- and
   *     output-files.
   * @return the run config for that run request
   * @throws IOException if an I/O error occurs while writing a config file
   */
  public RunConfig createRunConfig(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws IOException {
    Path tmpDirectory = runDirectory.getCurrentInputDirectory();

    Path programFile = tmpDirectory.resolve("program.c");
    createFile(programFile, request.programCode());

    Path configFile = runDirectory.getConfigLocation();
    writeFile(configFile, request.config());

    RunConfig.MachineModel machineModel = map(request.machineModel());

    String entryFunction =
        request.entryFunction().isBlank() ? DEFAULT_ANALYSIS_ENTRY : request.entryFunction();

    RunConfig.RunConfigBuilder runConfig =
        RunConfig.builder()
            .programFile(programFile)
            .cpacheckerConfig(configFile)
            .machineModel(machineModel)
            .entryFunction(entryFunction);

    if (!Strings.isNullOrEmpty(request.specification())) {
      Path specFile = tmpDirectory.resolve("specification.properties");
      createFile(specFile, request.specification());
      runConfig.specification(specFile);
    }

    if (!Strings.isNullOrEmpty(request.witness())) {
      Path witness = tmpDirectory.resolve("witness.graphml");
      createFile(witness, request.witness());
      runConfig.witness(witness);
    }

    for (var option : request.textOptions()) {
      String optionValue = String.join(CPACHECKER_OPTION_VALUE_DELIMITER, option.value());
      RunConfig.Option configOption = new RunConfig.Option(option.key(), optionValue);
      runConfig.additionalOption(configOption);
    }

    for (var option : request.fileOptions()) {
      List<String> files = createFilesWithContent(option, tmpDirectory);
      String optionValue = String.join(CPACHECKER_OPTION_VALUE_DELIMITER, files);
      RunConfig.Option configOption = new RunConfig.Option(option.key(), optionValue);
      runConfig.additionalOption(configOption);
    }

    return runConfig.build();
  }

  private List<String> createFilesWithContent(StartRun.FileOption option, Path tmpDirectory)
      throws IOException {
    List<ByteBuffer> contents = option.content();
    List<String> files = new ArrayList<>(contents.size());
    for (int i = 0; i < contents.size(); i++) {
      ByteBuffer content = contents.get(i);
      // add counter to avoid duplicate file names
      Path optionFile = tmpDirectory.resolve(getOptionFileName(option.key() + i));
      createFile(optionFile, content);
      files.add(optionFile.toAbsolutePath().toString());
    }
    return files;
  }

  private String getOptionFileName(String name) {
    return "optionFile" + name;
  }

  private RunConfig.MachineModel map(StartRun.MachineModel machineModel) {
    return switch (machineModel) {
      case LINUX32 -> RunConfig.MachineModel.LINUX32;
      case LINUX64 -> RunConfig.MachineModel.LINUX64;
      case UNSPECIFIED, UNRECOGNIZED -> throw new AssertionError(
          "Unrecognized or missing machine model should be rejected in service");
    };
  }

  /**
   * In contrast to {@link #createFile(Path, String)} , this method will **NOT** fail if the file
   * already exists. This is intended for sitations where the file may already exist, e.g., when the
   * path is provided by the RunDirectory.
   *
   * @param file the file to write to
   * @param fileContent the content to write to the file
   * @throws IOException if an I/O error occurs while writing the file
   */
  private void writeFile(Path file, String fileContent) throws IOException {
    try {
      Files.createFile(file);
    } catch (FileAlreadyExistsException e) {
      // ignore
    }
    Files.writeString(file, fileContent, StandardCharsets.UTF_8);
  }

  private void createFile(Path file, String fileContent) throws IOException {
    if (file.toFile().exists()) {
      throw new IOException("Trying to write to new file " + file + ", but exists");
    }
    Files.createFile(file);
    Files.writeString(file, fileContent, StandardCharsets.UTF_8);
  }

  private void createFile(Path file, ByteBuffer fileContent) throws IOException {
    if (file.toFile().exists()) {
      throw new IOException("Trying to write to new file " + file + ", but exists");
    }
    Files.createFile(file);
    try (FileOutputStream out = new FileOutputStream(file.toFile())) {
      out.getChannel().write(fileContent);
    }
  }
}
