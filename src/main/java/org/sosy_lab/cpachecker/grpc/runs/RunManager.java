// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.requests.CancelRun;
import org.sosy_lab.cpachecker.grpc.requests.CloseRun;
import org.sosy_lab.cpachecker.grpc.requests.GetFile;
import org.sosy_lab.cpachecker.grpc.requests.GetRun;
import org.sosy_lab.cpachecker.grpc.requests.ListRuns;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.requests.WaitRun;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;

/** Manager of {@link Run runs}. */
@ApplicationScoped
public class RunManager {
  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private static final AtomicInteger RUN_COUNTER = new AtomicInteger(0);
  private static final AtomicInteger FINISHED_RUN_COUNTER = new AtomicInteger(0);
  private final RunStore runStore;

  private final RunFactory runFactory;
  private final CpacheckerSupport cpacheckerSupport;

  @Inject
  RunManager(RunFactory runFactory, CpacheckerSupport cpacheckerSupport, RunStore runStore) {
    this.runFactory = runFactory;
    this.cpacheckerSupport = cpacheckerSupport;
    this.runStore = runStore;
  }

  /**
   * Commissions a new run.
   *
   * @param request request for the new run
   * @return the initial state of the new run
   * @throws RunException if the request is invalid or the run failed to start
   */
  public RunState commissionRun(StartRun request) throws RunException {
    // These error-checks have one peculiarity:
    // An invalid configuration file provokes directly an error,
    // but an invalid program file (e.g. with bogus content) produces a RunResult with verdict
    // 'ERROR'. This is on purpose because we can never clearly differentiate correct from incorrect
    // content from the outside:
    // An invalid configuration file always is the user's fault, because
    // we know that CPAchecker supports the full configuration-file format.
    // But CPAchecker may fail to parse a program file that actually is valid code, because the
    // parsing frontend may miss the support for some program feature. In this case, we do not want
    // to falsely blame the user.
    List<String> issuesWithRequest = cpacheckerSupport.getIssuesWithRequest(request);
    if (!issuesWithRequest.isEmpty()) {
      throw new RunException("Invalid request. Issues: " + String.join(", ", issuesWithRequest));
    }
    String name = nextName();
    Run run = runFactory.create(name);
    runStore.put(run);
    run.start(request);
    return run.getState();
  }

  private String nextName() {
    return "Run/" + RUN_COUNTER.incrementAndGet();
  }

  /** Returns the count of finished runs. */
  public Integer getFinishedRunCount() {
    return FINISHED_RUN_COUNTER.get();
  }

  /** Returns the count of started runs. */
  public Integer getRunCount() {
    return RUN_COUNTER.get();
  }

  /**
   * Returns the state of the queried run, if it exists.
   *
   * @param getRequest request for the run
   * @return the state of the queried run, if it exists. Otherwise, {@link Optional#empty()}.
   */
  public Optional<RunState> getRun(GetRun getRequest) {
    String name = getRequest.name();
    return Optional.ofNullable(runStore.get(name)).map(Run::getState);
  }

  /**
   * Returns all currently existing runs. This may include runs already finished, but not cleaned up
   * yet, and runs that are still running.
   *
   * @param listRunsRequest request for listing runs
   * @return the list of all currently known runs
   */
  public List<RunState> listRuns(ListRuns listRunsRequest) {
    return runStore.getAll().stream()
        .map(Run::getState)
        .sorted(Comparator.comparing(RunState::getTimestamp))
        .toList();
  }

  /**
   * Cancels a run.
   *
   * @param cancelRequest request for cancelling the run
   * @return the state of the run before cancellation
   */
  public Optional<RunState> cancelRun(CancelRun cancelRequest) {
    Run toCancel = runStore.get(cancelRequest.name());
    Optional<RunState> stateBeforeCancellation = Optional.ofNullable(toCancel).map(Run::getState);
    if (toCancel != null) {
      toCancel.stop();
    }
    return stateBeforeCancellation;
  }

  /**
   * Waits for a run to finish.
   *
   * @param waitRun the request
   * @return the state of the run after it finished or after the timeout {@link WaitRun#timeout()}
   *     expired, whatever comes first. If the Thread is interrupted while waiting, the current run
   *     state is returned.
   */
  public Optional<RunState> waitRun(WaitRun waitRun) {
    Run toWaitFor = runStore.get(waitRun.name());
    if (toWaitFor != null) {
      try {
        RunState latestState = toWaitFor.waitUntilFinished(waitRun.timeout().toMillis());
        // short-circuit: do not re-poll the state because we already have the latest state.
        return Optional.of(latestState);
      } catch (InterruptedException e) {
        // interrupted: return the current state by falling through
      }
    }
    return Optional.ofNullable(toWaitFor).map(Run::getState);
  }

  /**
   * Returns the output file of a run.
   *
   * @param getFileRequest the request
   * @return the content of the output file, if it exists. Otherwise, {@link Optional#empty()}.
   */
  public Optional<ByteBuffer> getOutputFile(GetFile getFileRequest) {
    Run run = runStore.get(getFileRequest.runName());
    return Optional.ofNullable(run).map(r -> r.getOutputFile(getFileRequest.fileName()));
  }

  /**
   * Closes a run and removes it from the store.
   *
   * @param closeRequest the request
   * @return the state of the run before closing
   */
  public Cpachecker.Void closeRun(CloseRun closeRequest) {
    Optional<Run> toClose = runStore.remove(closeRequest.name());
    if (toClose.isPresent()) {
      try {
        toClose.get().close();
      } catch (Exception e) {
        LOG.errorf(e, "Exception while closing run %s", closeRequest.name());
      }
    }
    FINISHED_RUN_COUNTER.incrementAndGet();
    return Cpachecker.Void.getDefaultInstance();
  }
}
