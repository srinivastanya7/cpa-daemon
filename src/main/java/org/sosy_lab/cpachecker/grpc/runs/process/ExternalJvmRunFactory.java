// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import io.quarkus.arc.DefaultBean;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunFactory;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** Factory that creates CPAchecker runs that run in an external JVM process. */
@Singleton
@DefaultBean
class ExternalJvmRunFactory implements RunFactory {

  private final CpacheckerSupport cpacheckerSupport;
  private final JvmOptions jvmOptions;
  private final ProcessOptions processOptions;
  private final RunConfigMapper configMapper;
  private final DirectoryManager directoryManager;
  private final CpacheckerProcessBuilder processBuilder;

  /**
   * Creates a new instance of this class.
   *
   * @param jvmOptions options for the JVM that runs CPAchecker
   * @param processOptions options for the process that runs CPAchecker
   * @param cpacheckerSupport utility class for handling CPAchecker
   * @param configMapper config mapper
   * @param directoryManager manager for working directories of CPAchecker runs
   * @param processBuilder builder for CPAchecker processes
   */
  @Inject
  ExternalJvmRunFactory(
      JvmOptions jvmOptions,
      ProcessOptions processOptions,
      CpacheckerSupport cpacheckerSupport,
      RunConfigMapper configMapper,
      DirectoryManager directoryManager,
      CpacheckerProcessBuilder processBuilder) {
    this.jvmOptions = jvmOptions;
    this.processOptions = processOptions;
    this.cpacheckerSupport = cpacheckerSupport;
    this.configMapper = configMapper;
    this.directoryManager = directoryManager;
    this.processBuilder = processBuilder;
  }

  @Override
  public Run create(String name) {
    return new ExternalJvmRun(
        name,
        jvmOptions,
        processOptions,
        configMapper,
        cpacheckerSupport,
        directoryManager,
        processBuilder);
  }
}
