// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import jakarta.annotation.Nullable;
import jakarta.inject.Singleton;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
class MapBasedRunStore implements RunStore {
  private final Map<String, Run> runs = new ConcurrentHashMap<>();

  @Override
  public void put(Run run) {
    runs.put(run.getName(), run);
  }

  @Override
  public @Nullable Run get(String name) {
    return runs.get(name);
  }

  @Override
  public Collection<Run> getAll() {
    return runs.values();
  }

  @Override
  public void clear() {
    runs.clear();
  }

  @Override
  public Optional<Run> remove(String name) {
    return Optional.ofNullable(runs.remove(name));
  }
}
