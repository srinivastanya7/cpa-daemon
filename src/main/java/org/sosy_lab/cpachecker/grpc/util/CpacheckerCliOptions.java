// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import com.google.common.collect.ImmutableList;
import java.nio.file.Path;
import java.util.Objects;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;

class CpacheckerCliOptions {

  ImmutableList<String> createCommandLineOptions(RunConfig config, Path outputDirectory) {
    ImmutableList.Builder<String> command = ImmutableList.builder();

    add(command, pathToString(config.getProgramFile()));
    add(command, "-outputpath", pathToString(outputDirectory));
    add(command, "-config", pathToString(config.getCpacheckerConfig()));

    addIfNonNull(command, "-spec", config.getSpecification());
    for (RunConfig.Option option : config.getAdditionalOptions()) {
      add(command, "-setprop", option.key() + "=" + option.value());
    }
    switch (config.getMachineModel()) {
      case LINUX32 -> add(command, "-32");
      case LINUX64 -> add(command, "-64");
      default -> throw new AssertionError("Missing case statement: " + config.getMachineModel());
    }
    add(command, "-entryfunction", config.getEntryFunction());
    addIfNonNull(command, "-witness", config.getWitness());
    return command.build();
  }

  private void add(ImmutableList.Builder<String> command, String argument) {
    Objects.requireNonNull(argument);
    command.add(argument);
  }

  private void add(ImmutableList.Builder<String> command, String parameter, String value) {
    Objects.requireNonNull(parameter);
    Objects.requireNonNull(value);
    command.add(parameter);
    command.add(value);
  }

  private void addIfNonNull(ImmutableList.Builder<String> command, String parameter, Path value) {
    Objects.requireNonNull(parameter);
    if (value != null) {
      command.add(parameter);
      command.add(pathToString(value));
    }
  }

  private String pathToString(Path path) {
    return path.toAbsolutePath().toString();
  }
}
