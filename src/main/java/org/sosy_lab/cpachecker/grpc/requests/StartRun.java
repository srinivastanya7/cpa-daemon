// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.requests;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Request to start a new run.
 *
 * @param programCode the program code to analyze.
 * @param config the CPAchecker configuration to use. For options that use input files, {@link
 *     #fileOptions} must be used.
 * @param specification the specification to check.
 * @param textOptions the {@link TextOption TextOptions} to add to the configuration. These options
 *     override any options given in {@link #config}.
 * @param fileOptions the {@link FileOption FileOptions} to add to the configuration. These options
 *     can not be provided through {@link #config}.
 * @param machineModel the machine model to assume in the run.
 * @param entryFunction the entry function of the program analysis (example: "main").
 * @param witness the witness to validate, if witness validation is to be performed.
 * @see org.sosy_lab.cpachecker.grpc.runs.RunManager
 */
public record StartRun(
    String programCode,
    String config,
    String specification,
    List<TextOption> textOptions,
    List<FileOption> fileOptions,
    MachineModel machineModel,
    String entryFunction,
    String witness) {

  /** Machine model to assume in the run. */
  public enum MachineModel {
    UNRECOGNIZED,
    UNSPECIFIED,
    LINUX32,
    LINUX64
  }

  /**
   * A run option that has text as value. Example: <code>analysis.stopAfterError=true</code>.
   *
   * @param key the key of the option (for example <code>analysis.stopAfterError</code>)
   * @param value the text values of the option (for example <code>true</code>)
   */
  public record TextOption(String key, List<String> value) {}

  /**
   * A run option that has file content as value.
   *
   * <p>Example on the CPAchecker command-line: <code>
   * witness.validation.violation.config=cpachecker/configs/witnessValidation.properties</code>
   *
   * <p>Example as FileOption: <code>
   * FileOption(key: "witness.valdation.violation.config", content: &lt;byte content of
   * cpachecker/configs/witnessValidation.properties&gt;)</code>
   *
   * @param key the key of the option (for example <code>witness.validation.violation.config</code>)
   * @param content the byte contents of the input files
   */
  public record FileOption(String key, List<ByteBuffer> content) {}
}
