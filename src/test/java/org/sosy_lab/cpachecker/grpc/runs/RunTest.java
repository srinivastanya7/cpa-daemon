// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.oneOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.ENTRY_FUNCTION_DEFAULT;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getBasicRequest;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsBytes;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsString;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.testing.RunContext;

/** Unit tests for the interface contract of {@link Run}. */
public interface RunTest {

  String PROGRAM_VERDICT_TRUE = "safe.c";
  String PROGRAM_VERDICT_FALSE = "unsafe.c";
  String PROGRAM_WITH_FLOATS_VERDICT_FALSE = "floats_unsafe.c";
  String PROGRAM_INFINITE_RUN = "infiniteRun.c";
  String PROGRAM_RECURSION = "unsolvable.c";
  String PROGRAM_INVALID_C = "invalid_syntax.c";
  String SOME_PROGRAM = PROGRAM_VERDICT_TRUE;
  String CONFIG_WITHOUT_VERIFICATION = "generateCFA.properties";
  String CONFIG_WITH_VERIFICATION = "valueAnalysis.properties";
  String CONFIG_WITH_VERIFICATION_AND_FLOATS_LIBRARY = "predicateAnalysis.properties";
  String CONFIG_WITNESS_VALIDATION = "witnessValidation.properties";
  String CONFIG_WITH_VERIFICATION_FAILS_ON_RECURSION = "valueAnalysis-failOnRecursion.properties";
  String VIOLATION_WITNESS_CORRECT = "violationWitness-correct.graphml";
  String VIOLATION_WITNESS_INCORRECT = "violationWitness-incorrect.graphml";
  String CONFIG_EMPTY = "empty.properties";
  String CONFIG_INVALID = "invalid.properties";
  String SPECIFICATION = "unreach-call.spc";

  String OPTION_CFA_OUTPUT_KEY = "cfa.export";
  String OPTION_CFA_OUTPUT_VALUE = "true";
  String OPTION_CFA_OUTPUT_PATH_KEY = "cfa.file";
  String OPTION_CFA_OUTPUT_PATH_VALUE = "cfa.dot";
  String MISSING_FILE = "__this_file_does_not_exist.foobar";
  long WAIT_FOR_LONG_RUNNING_TESTS_IN_MILLIS = 5_000; // 5 seconds
  long WAIT_TIMEOUT_FOR_RUNS_IN_MILLIS = 10_000_000; // 10 000 seconds
  RunContext mutableRunContext = new RunContext();

  Run getRun();

  @BeforeEach
  default void setUp() throws IOException {
    mutableRunContext.setCurrentOutputDirectory(
        Files.createTempDirectory("cpa-daemon-test-output-"));
  }

  @AfterEach
  default void tearDown() throws IOException {
    mutableRunContext.tearDown();
  }

  private static Path getFile(String name) {
    return TestUtils.getRunnerFile(name);
  }

  @Test
  default void testStart_alreadyStarted() throws RunException {
    // given
    Run run = getRun();
    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when
    run.start(runRequest);

    // then
    assertThrows(IllegalStateException.class, () -> run.start(runRequest));
  }

  // The interface does not define what happens if 'stop' is called multiple times,
  // so we do not test this here.
  @Test
  default void testStop_notStarted() {
    // given
    Run run = getRun();

    // then
    assertThrows(IllegalStateException.class, run::stop);
  }

  @Test
  default void testStop_notDoneYet() throws RunException, InterruptedException {
    // given
    Run run = getRun();
    StartRun runRequest =
        getBasicRequest(PROGRAM_INFINITE_RUN, CONFIG_WITH_VERIFICATION, SPECIFICATION);
    run.start(runRequest);

    // when
    run.stop();
    // give the process some time to stop
    Thread.sleep(WAIT_FOR_LONG_RUNNING_TESTS_IN_MILLIS);
    RunState afterStop = run.getState();

    // then the run should be finished
    assertThat(afterStop.getActiveness(), equalTo(RunState.Activeness.FINISHED));
  }

  @Test
  default void testWaitUntilFinished_notStarted() {
    // given
    Run run = getRun();

    // then
    assertThrows(
        IllegalStateException.class, () -> run.waitUntilFinished(WAIT_TIMEOUT_FOR_RUNS_IN_MILLIS));
  }

  @Test
  default void testWaitUntilFinished_alreadyFinished() throws RunException, InterruptedException {
    // given
    Run run = getRun();
    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when
    run.start(runRequest);
    RunState finished1 = run.waitUntilFinished(WAIT_TIMEOUT_FOR_RUNS_IN_MILLIS);
    // 1 millisecond of timeout should be ok, because the run must already be finished.
    RunState finished2 = run.waitUntilFinished(1);

    // then the same state should be returned
    assertThat(finished1, equalTo(finished2));
    // and the run should be finished
    assertThat(finished1.getActiveness(), equalTo(RunState.Activeness.FINISHED));
  }

  @Test
  default void testGetName() {
    // given
    Run run = getRun();

    // then
    assertThat(run.getName(), not(blankOrNullString()));
  }

  @Test
  default void testGetOutputFile_exists() throws RunException, InterruptedException {
    // given
    Run run = getRun();
    StartRun prototype = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);
    StartRun runRequest =
        new StartRun(
            prototype.programCode(),
            prototype.config(),
            prototype.specification(),
            List.of(
                new StartRun.TextOption(OPTION_CFA_OUTPUT_KEY, List.of(OPTION_CFA_OUTPUT_VALUE)),
                new StartRun.TextOption(
                    OPTION_CFA_OUTPUT_PATH_KEY, List.of(OPTION_CFA_OUTPUT_PATH_VALUE))),
            List.of(),
            prototype.machineModel(),
            prototype.entryFunction(),
            prototype.witness());

    // when we start the run
    run.start(runRequest);
    // and give the run some time to stop
    Thread.sleep(WAIT_FOR_LONG_RUNNING_TESTS_IN_MILLIS);
    // and get the output file we explicitly specified
    ByteBuffer content = run.getOutputFile(OPTION_CFA_OUTPUT_PATH_VALUE);

    // then the content is not null and not empty
    assertThat(content, not(nullValue()));
    assertThat(content.remaining(), greaterThan(0));
    // and the content is valid UTF-8
    String contentString = StandardCharsets.UTF_8.decode(content).toString();
    // and it contains sane text: in our case, the CFA definition 'digraph CFA'
    assertThat(contentString, containsString("digraph CFA"));
  }

  @Test
  default void testGetOutputFile_missingReturnsNull() throws RunException, InterruptedException {
    // given
    Run run = getRun();
    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when we start the run
    run.start(runRequest);
    // and give the run some time to stop
    Thread.sleep(WAIT_FOR_LONG_RUNNING_TESTS_IN_MILLIS);
    // and get some output file that doesn't exist
    ByteBuffer content = run.getOutputFile(MISSING_FILE);

    // then the content is null
    assertThat(content, nullValue());
  }

  // we only test valid requests. The RunManager has to ensure that only valid requests are passed
  // on to the actual Runs.
  /** Tests different valid run requests. */
  @ParameterizedTest(name = "{0}")
  @MethodSource("provideValidRunArguments")
  default void run_with_valid_configurations(
      String testDescription, StartRun runRequest, RunResult.Verdict expectedVerdict)
      throws RunException, InterruptedException {
    synchronized (mutableRunContext) { // make sure the run context is not modified during the run
      // when
      Run run = getRun();
      RunState stateBeforeStart = run.getState();
      run.start(runRequest);
      RunState stateAfterStart = run.getState();
      RunState finishedState = run.waitUntilFinished(WAIT_TIMEOUT_FOR_RUNS_IN_MILLIS);

      // then
      checkStateBeforeStart(stateBeforeStart);

      // the state should keep the same name and timestamp over its full lifecycle
      assertThat(stateBeforeStart.getName(), equalTo(stateAfterStart.getName()));
      assertThat(stateAfterStart.getName(), equalTo(finishedState.getName()));
      assertThat(stateBeforeStart.getTimestamp(), equalTo(stateAfterStart.getTimestamp()));
      assertThat(stateAfterStart.getTimestamp(), equalTo(finishedState.getTimestamp()));

      // after starting, the state should not be pending anymore
      assertThat(
          stateAfterStart.getActiveness(),
          oneOf(RunState.Activeness.RUNNING, RunState.Activeness.FINISHED));
      assertThat(finishedState.getActiveness(), equalTo(RunState.Activeness.FINISHED));

      assertThat(
          "Verdict does not match expected verdict.\n"
              + "CPAchecker stdout: "
              + finishedState.getStdout()
              + "\nCPAchecker stderr: "
              + finishedState.getStderr(),
          finishedState.getResult().verdict(),
          equalTo(expectedVerdict));
    }
  }

  /**
   * Provides the run arguments for {@link #run_with_valid_configurations(String, StartRun,
   * RunResult.Verdict)}.
   */
  static Stream<Arguments> provideValidRunArguments() throws IOException {
    return Stream.of(
        Arguments.of(
            "valid config, no verification",
            getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null),
            RunResult.Verdict.DONE),
        Arguments.of(
            "valid config, verification verdict TRUE",
            getBasicRequest(PROGRAM_VERDICT_TRUE, CONFIG_WITH_VERIFICATION, SPECIFICATION),
            RunResult.Verdict.TRUE),
        Arguments.of(
            "valid config, validation with verdict TRUE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_FALSE)),
                /* config= */ getContentAsString(getFile(CONFIG_WITNESS_VALIDATION)),
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ getWitnessValidationOptions(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ "main",
                /* witness= */ null),
            RunResult.Verdict.TRUE),
        Arguments.of(
            "valid config, verification with floats",
            getBasicRequest(
                PROGRAM_WITH_FLOATS_VERDICT_FALSE,
                CONFIG_WITH_VERIFICATION_AND_FLOATS_LIBRARY,
                SPECIFICATION),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "valid config provided through separate options, verification verdict TRUE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_TRUE)),
                /* config= */ "",
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ getOptionsForValueAnalysis(),
                /* fileOptions= */ List.of(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ "main",
                /* witness= */ null),
            RunResult.Verdict.TRUE),
        Arguments.of(
            "valid restart config with multi-file option, verification verdict FALSE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_FALSE)),
                /* config= */ "analysis.restartAfterUnknown=true",
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ List.of(),
                // the first analysis of the restart algorithm should fail on recursion,
                // the second should skip it and succeed
                /* fileOptions= */ getRestartAlgorithmFiles(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ "main",
                /* witness= */ null),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "valid config, validation with verdict FALSE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_FALSE)),
                /* config= */ getContentAsString(getFile(CONFIG_WITNESS_VALIDATION)),
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ getWitnessValidationOptions(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ "main",
                /* witness= */ getContentAsString(getFile(VIOLATION_WITNESS_CORRECT))),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "valid config, validation with verdict TRUE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_FALSE)),
                /* config= */ getContentAsString(getFile(CONFIG_WITNESS_VALIDATION)),
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ getWitnessValidationOptions(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ "main",
                /* witness= */ getContentAsString(getFile(VIOLATION_WITNESS_INCORRECT))),
            RunResult.Verdict.TRUE),
        Arguments.of(
            "valid config, verification verdict FALSE",
            getBasicRequest(PROGRAM_VERDICT_FALSE, CONFIG_WITH_VERIFICATION, SPECIFICATION),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "valid config, valid additional parameter, verification verdict FALSE",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_RECURSION)),
                /* config= */ getContentAsString(
                    getFile(CONFIG_WITH_VERIFICATION_FAILS_ON_RECURSION)),
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                // we use a config that fails on recursion, but additionally tell the config to skip
                // recursion. If this is handled correctly by the runner, the analysis finishes
                // successfully.
                /* textOptions= */ List.of(
                    new StartRun.TextOption("cpa.callstack.skipRecursion", List.of("true"))),
                /* fileOptions= */ List.of(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ ENTRY_FUNCTION_DEFAULT,
                /* witness= */ null),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "valid config, verification verdict UNKNOWN",
            getBasicRequest(
                PROGRAM_RECURSION, CONFIG_WITH_VERIFICATION_FAILS_ON_RECURSION, SPECIFICATION),
            RunResult.Verdict.UNKNOWN),
        Arguments.of(
            "empty config",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(PROGRAM_VERDICT_FALSE)),
                /* config= */ getContentAsString(getFile(CONFIG_EMPTY)),
                /* specification= */ getContentAsString(getFile(SPECIFICATION)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                /* machineModel= */ StartRun.MachineModel.LINUX32,
                /* entryFunction= */ ENTRY_FUNCTION_DEFAULT,
                /* witness= */ null),
            RunResult.Verdict.FALSE),
        Arguments.of(
            "invalid program",
            getBasicRequest(PROGRAM_INVALID_C, CONFIG_WITH_VERIFICATION, SPECIFICATION),
            RunResult.Verdict.ERROR),
        Arguments.of(
            "inconsistent parameters: config without specification CPA, but specification",
            getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, SPECIFICATION),
            RunResult.Verdict.ERROR));
  }

  private static List<StartRun.FileOption> getRestartAlgorithmFiles() throws IOException {
    return List.of(
        new StartRun.FileOption(
            "restartAlgorithm.configFiles",
            List.of(
                getContentAsBytes(getFile(CONFIG_INVALID)),
                getContentAsBytes(getFile(CONFIG_WITH_VERIFICATION)))));
  }

  private static List<StartRun.TextOption> getOptionsForValueAnalysis() {
    List<String> cpas =
        List.of(
            "cpa.location.LocationCPA",
            "cpa.callstack.CallstackCPA",
            "cpa.functionpointer.FunctionPointerCPA",
            "cpa.value.ValueAnalysisCPA",
            "$specification");
    return List.of(
        getTextOption("analysis.alwaysStoreCounterexamples", "true"),
        getTextOption("cpa.composite.aggregateBasicBlocks", "true"),
        getTextOption("cpa", "cpa.arg.ARGCPA"),
        getTextOption("ARGCPA.cpa", "cpa.composite.CompositeCPA"),
        getTextOption("CompositeCPA.cpas", cpas),
        getTextOption("analysis.summaryEdges", "true"),
        getTextOption("analysis.checkCounterexamples", "false"),
        getTextOption("cpa.callstack.skipRecursion", "true"),
        getTextOption("analysis.traversal.order", "bfs"),
        getTextOption("analysis.traversal.useReversePostorder", "false"),
        getTextOption("analysis.traversal.useCallstack", "false"));
  }

  private static StartRun.TextOption getTextOption(String key, String value) {
    return new StartRun.TextOption(key, List.of(value));
  }

  private static StartRun.TextOption getTextOption(String key, List<String> value) {
    return new StartRun.TextOption(key, value);
  }

  private void checkStateBeforeStart(RunState state) {
    // the run should not start automatically after creation, but only when
    // #start is called.
    assertThat(state.getActiveness(), equalTo(RunState.Activeness.PENDING));
    assertThat(state.getName(), not(blankOrNullString()));
    assertThat(state.getTimestamp(), greaterThan(0L));
    assertThat(state.getStdout(), blankOrNullString());
    assertThat(state.getStderr(), blankOrNullString());
    assertThat(state.getResult(), nullValue());
  }

  private static List<StartRun.FileOption> getWitnessValidationOptions() throws IOException {
    return List.of(
        new StartRun.FileOption(
            "witness.validation.violation.config",
            List.of(getContentAsBytes(getFile("witnessValidation.properties")))));
  }
}
