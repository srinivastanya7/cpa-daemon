// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.lenient;

import java.io.IOException;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

@ExtendWith(MockitoExtension.class)
class NativeBinaryRunTest implements AbstractProcessRunTest {
  private static final String DEFAULT_NAME = "someName";
  private static final int DEFAULT_TIMELIMIT = 60; // seconds
  private static final Path CPACHECKER_DIRECTORY = Path.of("build/cpachecker");
  private static final ProcessOptions DEFAULT_PROCESS_OPTIONS =
      new ProcessOptions(CPACHECKER_DIRECTORY, DEFAULT_TIMELIMIT);

  private final RunConfigMapper configMapper = new RunConfigMapper();
  private final CpacheckerSupport cpacheckerSupport = new CpacheckerSupport();
  private final Async async = new Async();
  private final CpacheckerProcessBuilder processBuilder = new CpacheckerProcessBuilder(async);

  private final Path nativeBinary = Path.of("build/cpachecker-native/cpachecker");

  @Mock private DirectoryManager directoryManager;

  @BeforeEach
  void setUpRunDirectory() throws IOException {
    Path tempPath = mutableRunContext.getCurrentOutputDirectory();
    assertThat(tempPath, notNullValue());
    DirectoryManager.RunDirectory runDirectory = TestUtils.getRunDirectory(tempPath, tempPath);
    // these stubs may not be used (e.g., in case a test expects an early error).
    // But the expected return values are the same for all tests, so define it here instead of
    lenient().when(directoryManager.createNewRunDirectory()).thenReturn(runDirectory);
  }

  @Override
  public AbstractProcessRun getRun() {
    return new NativeBinaryRun(
        DEFAULT_NAME,
        DEFAULT_PROCESS_OPTIONS,
        configMapper,
        cpacheckerSupport,
        directoryManager,
        processBuilder,
        nativeBinary);
  }
}
