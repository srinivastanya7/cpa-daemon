// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DirectoryManagerTest {

  private DirectoryManager manager;

  @BeforeEach
  void setUp() {
    manager = new DirectoryManager();
  }

  @Test
  void testCreateNewRunDirectory_createsInputDirectory() throws IOException {
    try (DirectoryManager.RunDirectory directory = manager.createNewRunDirectory()) {
      Path inputDir = directory.getCurrentInputDirectory();

      assertThat(Files.exists(inputDir), is(true));
      assertThat(Files.isDirectory(inputDir), is(true));
      assertThat(Files.isWritable(inputDir), is(true));
    }
  }

  @Test
  void testCreateNewRunDirectory_createsOutputDirectory() throws IOException {
    try (DirectoryManager.RunDirectory directory = manager.createNewRunDirectory()) {
      Path outputDir = directory.getCurrentOutputDirectory();

      assertThat(Files.exists(outputDir), is(true));
      assertThat(Files.isDirectory(outputDir), is(true));
      assertThat(Files.isWritable(outputDir), is(true));
    }
  }

  @Test
  void testCreateNewRunDirectory_inputAndOutputAreSeparate() throws IOException {
    try (DirectoryManager.RunDirectory directory = manager.createNewRunDirectory()) {
      Path outputDir = directory.getCurrentOutputDirectory();
      Path inputDir = directory.getCurrentInputDirectory();

      assertThat(outputDir, not(is(inputDir)));
    }
  }

  @Test
  void testCreateNewRunDirectory_deletesDirectoriesOnClose() throws IOException {
    final Path outputDir;
    final Path inputDir;

    try (DirectoryManager.RunDirectory directory = manager.createNewRunDirectory()) {
      outputDir = directory.getCurrentOutputDirectory();
      inputDir = directory.getCurrentInputDirectory();
    }

    // after close, the input- and output-directory should not exist anymore
    assertThat(Files.exists(outputDir), is(false));
    assertThat(Files.exists(inputDir), is(false));
  }

  @Test
  void testRunDirectory_setupCpacheckerConfigs() throws IOException {
    try (DirectoryManager.RunDirectory directory = manager.createNewRunDirectory()) {
      final Path oldConfigLoc = directory.getConfigLocation();
      directory.makeCpacheckerConfigsAvailable();
      Path inputDir = directory.getCurrentInputDirectory();
      final Path configDir = inputDir.resolve("config");

      assertThat(Files.exists(configDir), is(true));
      assertThat(Files.isDirectory(configDir), is(true));
      assertThat(Files.isReadable(configDir), is(true));
      assertThat(directory.getConfigLocation(), is(not(nullValue())));
      assertThat(directory.getConfigLocation(), is(not(equalTo(oldConfigLoc))));
      assertThat(Files.exists(configDir.resolve("svcomp23.properties")), is(true));
      assertThat(
          Files.exists(configDir.resolve("components/kInduction/kInduction.properties")), is(true));
    }
  }
}
