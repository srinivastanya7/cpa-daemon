// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.oneOf;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.google.protobuf.ByteString;
import com.google.protobuf.util.Durations;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.requests.CancelRun;
import org.sosy_lab.cpachecker.grpc.requests.GetFile;
import org.sosy_lab.cpachecker.grpc.requests.GetRun;
import org.sosy_lab.cpachecker.grpc.requests.ListRuns;
import org.sosy_lab.cpachecker.grpc.requests.WaitRun;
import org.sosy_lab.cpachecker.grpc.runs.RunManager;
import org.sosy_lab.cpachecker.grpc.runs.RunState;

@ExtendWith(MockitoExtension.class)
class CpacheckerRunManagerServiceTest {
  private static final String RUN_NAME = "Run/42";
  private static final String OUTPUT_FILE_NAME_1 = "output.txt";
  private static final String OUTPUT_FILE_NAME_2 = "statistics.txt";
  private static final Cpachecker.GetRunRequest REQUEST_GET_EXTERNAL =
      Cpachecker.GetRunRequest.newBuilder().setName(RUN_NAME).build();
  private static final GetRun REQUEST_GET_INTERNAL = new GetRun(RUN_NAME);
  private static final Cpachecker.ListRunsRequest REQUEST_LIST_EXTERNAL =
      Cpachecker.ListRunsRequest.newBuilder().build();
  private static final ListRuns REQUEST_LIST_INTERNAL = new ListRuns();
  private static final Cpachecker.CancelRunRequest REQUEST_CANCEL_EXTERNAL =
      Cpachecker.CancelRunRequest.newBuilder().setName(RUN_NAME).build();
  private static final CancelRun REQUEST_CANCEL_INTERNAL = new CancelRun(RUN_NAME);

  private static final Cpachecker.WaitRunRequest REQUEST_WAIT_EXTERNAL =
      Cpachecker.WaitRunRequest.newBuilder()
          .setName(RUN_NAME)
          .setTimeout(Durations.fromSeconds(9999))
          .build();
  private static final WaitRun REQUEST_WAIT_INTERNAL =
      new WaitRun(RUN_NAME, Duration.ofSeconds(9999));

  private static final Cpachecker.GetFileRequest REQUEST_GET_SINGLE_FILE_EXTERNAL =
      Cpachecker.GetFileRequest.newBuilder()
          .setRunName(RUN_NAME)
          .addFileName(OUTPUT_FILE_NAME_1)
          .build();
  private static final GetFile REQUEST_GET_SINGLE_FILE_INTERNAL =
      new GetFile(RUN_NAME, OUTPUT_FILE_NAME_1);

  private static final Cpachecker.GetFileRequest REQUEST_GET_MULTIPLE_FILES_EXTERNAL =
      Cpachecker.GetFileRequest.newBuilder()
          .setRunName(RUN_NAME)
          .addFileName(OUTPUT_FILE_NAME_1)
          .addFileName(OUTPUT_FILE_NAME_2)
          .build();
  private static final List<GetFile> REQUESTS_GET_MULTIPLE_FILES_INTERNAL =
      List.of(new GetFile(RUN_NAME, OUTPUT_FILE_NAME_1), new GetFile(RUN_NAME, OUTPUT_FILE_NAME_2));

  private static final long TIMESTAMP = 2132123;
  private static final RunState RUN_STARTED =
      RunState.builder()
          .name(RUN_NAME)
          .timestamp(TIMESTAMP)
          .activeness(RunState.Activeness.RUNNING)
          .build();

  private static final Cpachecker.Run RUN_WITHOUT_DETAILS =
      Cpachecker.Run.newBuilder().setName(RUN_NAME).setStatus(Cpachecker.RunStatus.RUNNING).build();
  private static final Cpachecker.RunDetails RUN_DETAILS =
      Cpachecker.RunDetails.newBuilder().build();
  private static final Cpachecker.Run RUN_WITH_DETAILS =
      Cpachecker.Run.newBuilder(RUN_WITHOUT_DETAILS).setDetails(RUN_DETAILS).build();

  @Mock private RunResponseMapper resultMapper;
  @Mock private RunManager runManager;
  private RunManagerService service;

  @BeforeEach
  void setUp() {
    service = new RunManagerService(runManager, resultMapper);
  }

  @Test
  void testGetRun_exists() {
    // given
    when(runManager.getRun(REQUEST_GET_INTERNAL)).thenReturn(Optional.of(RUN_STARTED));
    when(resultMapper.toRun(RUN_STARTED)).thenReturn(RUN_WITHOUT_DETAILS);
    when(resultMapper.mapDetails(RUN_STARTED)).thenReturn(RUN_DETAILS);

    // when
    Cpachecker.RunResponse response = service.getRun(REQUEST_GET_EXTERNAL).await().indefinitely();

    // then the response has a run (this also means that getRun was called)
    assertThat(response.hasRun(), is(true));
    // and it is the expected run with details added
    assertThat(response.getRun(), is(RUN_WITH_DETAILS));
  }

  @Test
  void testGetRun_missing() {
    // given
    when(runManager.getRun(REQUEST_GET_INTERNAL)).thenReturn(Optional.empty());

    // when
    Cpachecker.RunResponse response = service.getRun(REQUEST_GET_EXTERNAL).await().indefinitely();

    // then the response has an error
    assertThat(response.hasError(), is(true));
    // and the error is not a blank message
    assertThat(response.getError(), not(blankOrNullString()));
  }

  @Test
  void testListRuns() {
    // given
    var runs = List.of(RUN_STARTED);
    when(runManager.listRuns(REQUEST_LIST_INTERNAL)).thenReturn(runs);
    when(resultMapper.toListRunsResponse(runs))
        .thenReturn(Cpachecker.ListRunsResponse.newBuilder().addRuns(RUN_WITHOUT_DETAILS).build());
    // details may or may not be added
    lenient().when(resultMapper.mapDetails(RUN_STARTED)).thenReturn(RUN_DETAILS);

    // when
    Cpachecker.ListRunsResponse response =
        service.listRuns(REQUEST_LIST_EXTERNAL).await().indefinitely();

    // then the response has a list of runs (this also means that listRuns was called)
    assertThat(response.getRunsCount(), is(1));
    // and it is the expected list of runs (with or without details)
    assertThat(response.getRunsList(), contains(oneOf(RUN_WITHOUT_DETAILS, RUN_WITH_DETAILS)));
  }

  @Test
  void testCancelRun_exists() {
    // given
    when(runManager.cancelRun(REQUEST_CANCEL_INTERNAL)).thenReturn(Optional.of(RUN_STARTED));
    when(resultMapper.toRun(RUN_STARTED)).thenReturn(RUN_WITHOUT_DETAILS);
    // details may or may not be added
    lenient().when(resultMapper.mapDetails(RUN_STARTED)).thenReturn(RUN_DETAILS);

    // when
    Cpachecker.RunResponse response =
        service.cancelRun(REQUEST_CANCEL_EXTERNAL).await().indefinitely();

    // then the response has a run (this also means that cancelRun was called)
    assertThat(response.hasRun(), is(true));
    // and it is the expected run, with our without details
    assertThat(response.getRun(), oneOf(RUN_WITH_DETAILS, RUN_WITHOUT_DETAILS));
  }

  @Test
  void testCancelRun_missing() {
    // given
    when(runManager.cancelRun(REQUEST_CANCEL_INTERNAL)).thenReturn(Optional.empty());

    // when
    Cpachecker.RunResponse response =
        service.cancelRun(REQUEST_CANCEL_EXTERNAL).await().indefinitely();

    // then the response has an error
    assertThat(response.hasError(), is(true));
    // and the error is not a blank message
    assertThat(response.getError(), not(blankOrNullString()));
  }

  @Test
  void testWaitRun_missingRun() {
    // given
    when(runManager.waitRun(REQUEST_WAIT_INTERNAL)).thenReturn(Optional.empty());

    // when
    Cpachecker.RunResponse response = service.waitRun(REQUEST_WAIT_EXTERNAL).await().indefinitely();

    // then the response has an error
    assertThat(response.hasError(), is(true));
    // and the error is not a blank message
    assertThat(response.getError(), not(blankOrNullString()));
  }

  @Test
  void testWaitRun_existingRun() {
    // given
    when(runManager.waitRun(REQUEST_WAIT_INTERNAL)).thenReturn(Optional.of(RUN_STARTED));
    when(resultMapper.toRun(RUN_STARTED)).thenReturn(RUN_WITHOUT_DETAILS);
    when(resultMapper.mapDetails(RUN_STARTED)).thenReturn(RUN_DETAILS);

    // when
    Cpachecker.RunResponse response = service.waitRun(REQUEST_WAIT_EXTERNAL).await().indefinitely();

    // then the response has a run (this also means that waitRun was called)
    assertThat(response.hasRun(), is(true));
    // and it is the expected run with details added
    assertThat(response.getRun(), is(RUN_WITH_DETAILS));
  }

  @Test
  void testGetFile_singleExistingFile() {
    // given
    String fileContent = "file content";
    when(runManager.getOutputFile(REQUEST_GET_SINGLE_FILE_INTERNAL))
        .thenReturn(Optional.of(toByteBuffer(fileContent)));

    // when
    Cpachecker.GetFileResponse response =
        service.getFile(REQUEST_GET_SINGLE_FILE_EXTERNAL).await().indefinitely();

    // then the response has a single result element
    assertThat(response.getResultCount(), is(1));
    // and it has the file name we asked for
    assertThat(
        response.getResult(0).getFileName(), is(REQUEST_GET_SINGLE_FILE_EXTERNAL.getFileName(0)));
    // and it is the expected file content
    String responseContent = toString(response.getResult(0).getContent());
    assertThat(responseContent, is(fileContent));
  }

  @Test
  void testGetFile_singleMissingFile() {
    // given
    when(runManager.getOutputFile(REQUEST_GET_SINGLE_FILE_INTERNAL)).thenReturn(Optional.empty());

    // when
    Cpachecker.GetFileResponse response =
        service.getFile(REQUEST_GET_SINGLE_FILE_EXTERNAL).await().indefinitely();

    // then the response has a single result element
    assertThat(response.getResultCount(), is(1));
    // and it has the file name we asked for
    assertThat(
        response.getResult(0).getFileName(), is(REQUEST_GET_SINGLE_FILE_EXTERNAL.getFileName(0)));
    // and it is the expected file content
    assertThat(response.getResult(0).getError(), not(blankOrNullString()));
  }

  @Test
  void testGetFile_multipleExistingFiles() {
    // given each internal GetFile request returns some valid file content
    Map<String, String> namesAndContents = new HashMap<>();
    for (int i = 0; i < REQUESTS_GET_MULTIPLE_FILES_INTERNAL.size(); i++) {
      String content = "file content " + i;
      GetFile request = REQUESTS_GET_MULTIPLE_FILES_INTERNAL.get(i);
      namesAndContents.put(request.fileName(), content);
      when(runManager.getOutputFile(request)).thenReturn(Optional.of(toByteBuffer(content)));
    }

    // when
    Cpachecker.GetFileResponse response =
        service.getFile(REQUEST_GET_MULTIPLE_FILES_EXTERNAL).await().indefinitely();

    // then the response has the same number of result elements as the request
    assertThat(
        response.getResultCount(), is(REQUEST_GET_MULTIPLE_FILES_EXTERNAL.getFileNameCount()));
    // and each result element has
    for (int i = 0; i < REQUEST_GET_MULTIPLE_FILES_EXTERNAL.getFileNameCount(); i++) {
      Cpachecker.MaybeFile result = response.getResult(i);
      String fileName = result.getFileName();

      // a name that was in the request
      assertThat(namesAndContents.containsKey(fileName), is(true));
      // and the corresponding content
      String expectedContent = namesAndContents.get(fileName);
      String responseContent = toString(result.getContent());
      assertThat(responseContent, is(expectedContent));
    }
  }

  @Test
  void testGetFile_multipleFilesSomeMissing() {
    // given the first internal GetFile request returns no file content
    GetFile requestForMissing = REQUESTS_GET_MULTIPLE_FILES_INTERNAL.get(0);
    when(runManager.getOutputFile(requestForMissing)).thenReturn(Optional.empty());
    // and the other internal GetFile requests return a valid file content
    Map<String, String> namesAndContents = new HashMap<>();
    for (int i = 1; i < REQUESTS_GET_MULTIPLE_FILES_INTERNAL.size(); i++) {
      String content = "file content " + i;
      GetFile request = REQUESTS_GET_MULTIPLE_FILES_INTERNAL.get(i);
      namesAndContents.put(request.fileName(), content);
      when(runManager.getOutputFile(request)).thenReturn(Optional.of(toByteBuffer(content)));
    }

    // when
    Cpachecker.GetFileResponse response =
        service.getFile(REQUEST_GET_MULTIPLE_FILES_EXTERNAL).await().indefinitely();

    // then the response has the same number of result elements as the request
    assertThat(
        response.getResultCount(), is(REQUEST_GET_MULTIPLE_FILES_EXTERNAL.getFileNameCount()));
    // and each result element has
    Set<String> fileNamesInRequest =
        new HashSet<>(REQUEST_GET_MULTIPLE_FILES_EXTERNAL.getFileNameList());
    for (int i = 0; i < REQUEST_GET_MULTIPLE_FILES_EXTERNAL.getFileNameCount(); i++) {
      Cpachecker.MaybeFile result = response.getResult(i);
      String fileName = result.getFileName();

      // a name that was in the request
      assertThat(fileNamesInRequest, hasItem(fileName));

      // and
      if (fileName.equals(requestForMissing.fileName())) {
        // if the file is missing, the result contains an error
        assertThat(result.getError(), not(blankOrNullString()));
      } else {
        // otherwise, the result contains the corresponding content
        String expectedContent = namesAndContents.get(fileName);
        String responseContent = toString(result.getContent());
        assertThat(responseContent, is(expectedContent));
      }
    }
  }

  private ByteBuffer toByteBuffer(String value) {
    return ByteBuffer.wrap(value.getBytes(StandardCharsets.UTF_8));
  }

  private String toString(ByteString value) {
    return new String(value.toByteArray(), StandardCharsets.UTF_8);
  }
}
