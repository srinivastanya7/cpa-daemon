// This file is part of cpachecker-grpc,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpachecker-grpc/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: CC0-1.0

extern void reach_error();

int main() {
  unsigned long int i = 0xffffffff;
  i = i + 1;
  // safe for 64bit, but not for 32bit
  if (i == 0) {
    goto ERROR;
  }
  return (0);
  ERROR: reach_error();
  return (-1);
}

