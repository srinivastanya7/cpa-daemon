// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.BeforeAll;
import org.sosy_lab.cpachecker.grpc.proto.RunManagerGrpc;
import org.sosy_lab.cpachecker.grpc.proto.RunnerGrpc;

@QuarkusIntegrationTest
// Different quarkus profiles are selected at build.
// For example, to run this integration test with the native profile,
// run: `./gradlew quarkusIntTest -Dquarkus.profile=native`
class CpacheckerAllServiceIntegrationTest implements AllServiceIntegrationTest {

  private static RunnerGrpc.RunnerBlockingStub runner;
  private static RunManagerGrpc.RunManagerBlockingStub runManager;

  @BeforeAll
  static void setUpClients() {
    // this is a workaround for the missing Quarkus injection in integration tests; solution copied
    // and adjusted from https://github.com/quarkusio/quarkus/discussions/33599
    int port =
        ConfigProvider.getConfig().getValue("quarkus.grpc.clients.runner.port", Integer.class);
    String address =
        ConfigProvider.getConfig().getValue("quarkus.grpc.clients.runner.host", String.class);

    ManagedChannel channel = ManagedChannelBuilder.forAddress(address, port).usePlaintext().build();
    runner = RunnerGrpc.newBlockingStub(channel);
    runManager = RunManagerGrpc.newBlockingStub(channel);
  }

  @Override
  public RunnerGrpc.RunnerBlockingStub getRunner() {
    return runner;
  }

  @Override
  public RunManagerGrpc.RunManagerBlockingStub getRunManager() {
    return runManager;
  }

  @Override
  public long getWaitTimelimitInMillis() {
    return 60_000;
  }
}
