// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.oneOf;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsString;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.proto.RunnerGrpc;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;

interface RunnerServiceTest {

  Path PROGRAM_TRUE = TestUtils.getRunnerFile("safe.c");
  Path PROGRAM_TRUE_LARGE = TestUtils.getRunnerFile("safe-large.c");
  Path PROGRAM_FALSE = TestUtils.getRunnerFile("unsafe.c");
  Path PROGRAM_WITH_FLOATS = TestUtils.getRunnerFile("floats_unsafe.c");
  Path PROGRAM_UNSOLVABLE = TestUtils.getRunnerFile("unsolvable.c");
  Path PROGRAM_INVALID_SYNTAX = TestUtils.getRunnerFile("invalid_syntax.c");
  Path CONFIG_VERIFICATION = TestUtils.getRunnerFile("valueAnalysis.properties");
  Path CONFIG_VERIFICATION_WITH_FLOAT_LIBRARY =
      TestUtils.getRunnerFile("predicateAnalysis.properties");
  Path CONFIG_VERIFICATION_FOR_UNSOLVABLE =
      TestUtils.getRunnerFile("valueAnalysis-failOnRecursion.properties");
  Path CONFIG_NO_VERIFICATION = TestUtils.getRunnerFile("generateCFA.properties");
  Path CONFIG_VALIDATION = TestUtils.getRunnerFile("witnessValidation.properties");
  Path CONFIG_INVALID = TestUtils.getRunnerFile("invalid.properties");
  Path SPECIFICATION = TestUtils.getRunnerFile("unreach-call.spc");
  Path WITNESS_CORRECT = TestUtils.getRunnerFile("violationWitness-correct.graphml");
  Path WITNESS_INCORRECT = TestUtils.getRunnerFile("violationWitness-incorrect.graphml");
  Path WITNESS_INVALID_LARGE = TestUtils.getRunnerFile("invalid-large.graphml");
  Cpachecker.CpacheckerOption OPTION_ARGCPA =
      Cpachecker.CpacheckerOption.newBuilder()
          .setName("cpa")
          .setTextValue(Cpachecker.TextValues.newBuilder().addValues("cpa.arg.ARGCPA").build())
          .build();
  Cpachecker.CpacheckerOption OPTION_COMPOSITECPA =
      Cpachecker.CpacheckerOption.newBuilder()
          .setName("ARGCPA.cpa")
          .setTextValue(
              Cpachecker.TextValues.newBuilder().addValues("cpa.composite.CompositeCPA").build())
          .build();
  Cpachecker.CpacheckerOption OPTION_CPAS =
      Cpachecker.CpacheckerOption.newBuilder()
          .setName("CompositeCPA.cpas")
          .setTextValue(
              Cpachecker.TextValues.newBuilder()
                  .addValues("cpa.location.LocationCPA")
                  .addValues("cpa.callstack.CallstackCPA")
                  .addValues("cpa.value.ValueAnalysisCPA")
                  .addValues("$specification")
                  .build())
          .build();

  RunnerGrpc.RunnerBlockingStub getRunner();

  @ParameterizedTest(name = "{0}")
  @MethodSource("provideValidRequests")
  default void testStartRun_validRequests(
      String testName, Cpachecker.StartRunRequest request, Cpachecker.Verdict unused) {
    RunnerGrpc.RunnerBlockingStub runner = getRunner();
    Cpachecker.RunResponse runResponse = runner.startRun(request);

    assertThat(runResponse.hasRun(), is(true));
    Cpachecker.Run run = runResponse.getRun();
    assertThat(run.getName(), not(blankOrNullString()));
    assertThat(run.getStatus(), oneOf(Cpachecker.RunStatus.RUNNING, Cpachecker.RunStatus.FINISHED));
    assertThat(runResponse.hasError(), is(false));
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("provideInvalidRequests")
  default void testStartRun_invalidRequests(String testName, Cpachecker.StartRunRequest request) {
    RunnerGrpc.RunnerBlockingStub runner = getRunner();
    Cpachecker.RunResponse runResponse = runner.startRun(request);

    assertThat(runResponse.hasError(), is(true));
    assertThat(runResponse.getError(), not(blankOrNullString()));
  }

  static List<Arguments> provideValidRequests() throws IOException {
    List<Arguments> arguments =
        List.of(
            Arguments.of(
                "verification: result true",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "verification: result true (large program)",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE_LARGE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "verification: result false with LINUX32",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.FALSE),
            Arguments.of(
                "verification: result true with LINUX64",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX64)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            // Some configurations require a shared library for float analysis.
            // Test that this library is found and its use works.
            Arguments.of(
                "verification: result true for program with floats",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_WITH_FLOATS))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION_WITH_FLOAT_LIBRARY))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.FALSE),
            Arguments.of(
                "verification: result true with full config as separate options",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig("")
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .addAdditionalOptions(OPTION_ARGCPA)
                    .addAdditionalOptions(OPTION_COMPOSITECPA)
                    .addAdditionalOptions(OPTION_CPAS)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "verification: result unknown",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_UNSOLVABLE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION_FOR_UNSOLVABLE))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.UNKNOWN),
            Arguments.of(
                "verification: invalid program",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_INVALID_SYNTAX))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.ERROR),
            Arguments.of(
                "verification: invalid config",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_INVALID))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.ERROR),
            Arguments.of(
                "verification: invalid spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification(
                        getContentAsString(
                            PROGRAM_TRUE)) // use a program as spec; this is no valid spec
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.ERROR),
            Arguments.of(
                "verification: empty spec uses no spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setSpecification("")
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "verification: default spec uses no spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig(getContentAsString(CONFIG_VERIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "verification: restart config with multi-file option",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig("analysis.restartAfterUnknown=true")
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .addAdditionalOptions(getRestartConfigOption())
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.FALSE),
            Arguments.of(
                "no verification: default spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_NO_VERIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.DONE),
            Arguments.of(
                "no verification: explicitly empty spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_NO_VERIFICATION))
                    .setSpecification("")
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.DONE),
            Arguments.of(
                "no verification: given spec",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig(getContentAsString(CONFIG_NO_VERIFICATION))
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.ERROR),
            Arguments.of(
                "validation: witness rejected",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig("") // we only need the additional option below
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .setWitness(getContentAsString(WITNESS_INCORRECT))
                    .addAdditionalOptions(
                        Cpachecker.CpacheckerOption.newBuilder()
                            .setName("witness.validation.violation.config")
                            .setFileValue(readBytes(CONFIG_VALIDATION))
                            .build())
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.TRUE),
            Arguments.of(
                "validation: witness confirmed",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_FALSE))
                    .setConfig("") // we only need the additional option below
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .setWitness(getContentAsString(WITNESS_CORRECT))
                    .addAdditionalOptions(
                        Cpachecker.CpacheckerOption.newBuilder()
                            .setName("witness.validation.violation.config")
                            .setFileValue(readBytes(CONFIG_VALIDATION))
                            .build())
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.FALSE),
            // CPA-Daemon should be able to handle large witnesses and passes them on to CPAchecker.
            // We leave it to CPAchecker to check whether a witness is valid or invalid,
            // because only CPAchecker knows the allowed witness format.
            // Because of this, an invalid witness should not lead to an error in CPA-Daemon,
            // but to an error later.
            Arguments.of(
                "validation: witness invalid and large",
                Cpachecker.StartRunRequest.newBuilder()
                    .setProgramCode(getContentAsString(PROGRAM_TRUE))
                    .setConfig("")
                    .setSpecification(getContentAsString(SPECIFICATION))
                    .setMachineModel(Cpachecker.MachineModel.LINUX32)
                    .setWitness(getContentAsString(WITNESS_INVALID_LARGE))
                    .addAdditionalOptions(
                        Cpachecker.CpacheckerOption.newBuilder()
                            .setName("witness.validation.violation.config")
                            .setFileValue(readBytes(CONFIG_VALIDATION))
                            .build())
                    .build(),
                /* expectedVerdict= */ Cpachecker.Verdict.ERROR));

    // Test our tests: Make sure that we use every verdict in at least one test case
    int verdictPosition = 2;
    Set<Cpachecker.Verdict> testedVerdicts =
        arguments.stream()
            .map(a -> (Cpachecker.Verdict) a.get()[verdictPosition])
            .collect(Collectors.toSet());

    Set<Cpachecker.Verdict> existingVerdicts =
        Arrays.stream(Cpachecker.Verdict.values())
            .filter(v -> v != Cpachecker.Verdict.UNRECOGNIZED) // ignore protobuf verdict
            .collect(Collectors.toSet());
    assertThat(
        "We are missing module tests for runs with expected verdicts",
        testedVerdicts,
        equalTo(existingVerdicts));

    return arguments;
  }

  static Cpachecker.CpacheckerOption getRestartConfigOption() throws IOException {
    return Cpachecker.CpacheckerOption.newBuilder()
        .setName("restartAlgorithm.configFiles")
        .setFileValue(
            Cpachecker.FileValues.newBuilder()
                .addValues(readByteString(CONFIG_INVALID))
                .addValues(readByteString(CONFIG_VERIFICATION))
                .build())
        .build();
  }

  static Stream<Arguments> provideInvalidRequests() {
    return Stream.of(
        Arguments.of(
            "no machine model",
            Cpachecker.StartRunRequest.newBuilder()
                .setProgramCode(getContentAsString(PROGRAM_TRUE))
                .setConfig(getContentAsString(CONFIG_VERIFICATION))
                .setSpecification(getContentAsString(SPECIFICATION))
                .build()),
        Arguments.of(
            "empty config",
            Cpachecker.StartRunRequest.newBuilder()
                .setProgramCode(getContentAsString(PROGRAM_TRUE))
                .setConfig("")
                .setSpecification(getContentAsString(SPECIFICATION))
                .setMachineModel(Cpachecker.MachineModel.LINUX32)
                .build()),
        Arguments.of(
            "empty program",
            Cpachecker.StartRunRequest.newBuilder()
                .setProgramCode("")
                .setConfig(getContentAsString(CONFIG_VERIFICATION))
                .setSpecification(getContentAsString(SPECIFICATION))
                .setMachineModel(Cpachecker.MachineModel.LINUX32)
                .build()));
  }

  private static Cpachecker.FileValues readBytes(Path file) throws IOException {
    ByteString content = readByteString(file);
    return Cpachecker.FileValues.newBuilder().addValues(content).build();
  }

  private static ByteString readByteString(Path file) throws IOException {
    return ByteString.readFrom(Files.newInputStream(file));
  }
}
