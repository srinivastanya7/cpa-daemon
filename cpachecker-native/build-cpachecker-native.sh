#!/bin/bash

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail
set -x

PROJECT_ROOT=$(dirname $(realpath "$0"))
CPACHECKER_DIR=$(realpath "$1")
NATIVE_META_INFO_DIR="$2"
OUTPUT_DIR=$(realpath "$3")
cp "$CPACHECKER_DIR"/cpachecker.jar "$OUTPUT_DIR"/cpachecker.jar
(
  cd "$NATIVE_META_INFO_DIR"
  zip -gr "$OUTPUT_DIR"/cpachecker.jar ./META-INF
)
(
  cd "$OUTPUT_DIR"
  # CPAchecker requires a large heap (recommended: 10GB)
  # We use the --gc=G1 garbage collector because it is aimed to be better for large heap sizes.
  # We also set the minimum heap size to -R:MinHeapSize=5000M to make sure that CPAchecker has enough memory.
  native-image \
    --verbose \
    --no-fallback \
    --gc=G1 \
    -R:MinHeapSize=5000m \
    -H:+ReportExceptionStackTraces \
    -H:+AddAllCharsets \
    -g \
    -jar cpachecker.jar \
    -cp "$CPACHECKER_DIR"/lib/*:"$CPACHECKER_DIR"/lib/java/runtime/* \
    --enable-url-protocols=http,https
    # --bundle-create=native-build.nib \
    #-H:+BuildReport \
# docker run -it --rm $(pwd):/data ghcr.io/graalvm/native-image-community:20-ol9 bash
)

